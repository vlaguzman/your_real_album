require 'spec_helper'

describe ChosenAlbum do
  it { should validate_presence_of(:state) }

  it { should belong_to(:user) }

  it { should belong_to(:album) }
end
