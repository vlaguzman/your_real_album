require 'spec_helper'
include UserHelper

describe "User select a default album" do
  context "The user has logged in" do
    before do 
      @user = User.new(name: "Ronaldo", email:"ronaldo@gmail.com", password:"iamacrack")
      @album = Album.new(title: "I am a default album", description: "A default album to select")
    end
    context "The user has not selected the album" do
      it "should be able to add an album to the personal album list" do
        add_album(@album, @user)
        expect(@user.chosen_albums.size).to eq 1
      end
    end
    context "The user has selected the same album before" do
      it "should not be able to select the album again" do
        begin 
          add_album(@album, @user)
          add_album(@album, @user)
        rescue Exception => e
          result = :exception_handled
        end             
        expect("This album has been already selected").to eq e.message
      end
    end
  end
end
