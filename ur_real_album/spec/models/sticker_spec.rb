require 'spec_helper'

describe Sticker do
  it { should validate_presence_of(:name) }

  it { should belong_to(:album) }
end
