require 'spec_helper'

describe "A user create a new rule for an album" do
  context "the user has logged in" do
    context "the user has created or selected an album " do
      before do
        user = User.new(name: "Andres", email: "andres@gmail.com", password: "qwerty")
        @album = Album.new(title: "An album", description: "the best album ever")
        chosen_album = ChosenAlbum.new(state: "chosed")
        chosen_album.user = user
        chosen_album.album = @album
        @album.chosen_albums << chosen_album
        user.chosen_albums << chosen_album
        chosen_album.save!
        user.save!
        @album.save!
      end
      it "should create a new rule for the album" do
        rule = Rule.new(name: "The rule", description: "No pictures at the same place")
        rule.save!
        @album.rules << rule
        @album.save!
        expect(@album.rules.include?(rule)).to eq true
      end
    end
  end
end
