require 'spec_helper'

describe Album do
  it { should validate_presence_of(:title) }

  it { should validate_presence_of(:description)}

  it { should have_many(:stickers) }

  it { should have_many(:rules) }

  it { should have_many(:users)}

  it { should have_many(:chosen_albums)}
end
