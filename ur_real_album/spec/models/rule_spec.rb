require 'spec_helper'

describe Rule do
  it {should validate_presence_of(:name)}

  it {should validate_presence_of(:description)}

  it {should belong_to(:album)}

  it {should belong_to(:user)}
end
