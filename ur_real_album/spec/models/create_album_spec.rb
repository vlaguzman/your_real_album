require 'spec_helper'

describe "User create a new album" do
  context "The user has logged in" do
    it "should create the new album" do
      album = Album.new
      album.title = "Find girls with green pants"
      album.description = "My first album will be girls with green pants"
      album.save!
      album_searched = Album.first
      expect(album.title).to eq ("Find girls with green pants")
      expect(album.description).to eq ("My first album will be girls with green pants")
    end
  end
end
