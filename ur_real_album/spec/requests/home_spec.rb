require 'spec_helper'

describe 'home' do
  context "An album has created" do
    it "should show the tile and description of the album" do
      album = Album.new(title: "Girls with blue shoes", description: "you know what i mean")
      album.save!
      visit root_path
      expect(page).to have_content "Your real album"
      expect(page).to have_content "Girls with blue shoes"
      expect(page).to have_content "you know what i mean"
      expect(page.has_link?("Girls with blue shoes")).to eq true
      click_link_or_button("Girls with blue shoes")
      expect(page).to have_content "Girls with blue shoes"
      expect(page).to have_content "you know what i mean"
      #save_and_open_page
    end
  end
end
