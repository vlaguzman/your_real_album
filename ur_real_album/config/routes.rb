UrRealAlbum::Application.routes.draw do

  resources :albums
  resources :rules

  root to: 'home#index'

end
