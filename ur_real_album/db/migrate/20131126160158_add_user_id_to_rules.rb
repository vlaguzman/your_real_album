class AddUserIdToRules < ActiveRecord::Migration
  def up
    add_column :rules, :user_id, :integer
    add_index :rules, :user_id
  end
  def down
    remove_column :rules, :user_id
  end
end
