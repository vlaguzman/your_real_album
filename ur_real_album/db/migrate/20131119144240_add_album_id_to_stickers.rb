class AddAlbumIdToStickers < ActiveRecord::Migration
  def up
    add_column :stickers, :album_id, :integer
    add_index :stickers, :album_id
  end
  def down
    remove_column :stickers, :album_id
  end
end
