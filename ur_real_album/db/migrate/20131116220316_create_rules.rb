class CreateRules < ActiveRecord::Migration
  def up
    create_table :rules do |t|
      t.string :name
      t.string :description
    end
  end

  def down
  	drop_table :rules
  end
end
