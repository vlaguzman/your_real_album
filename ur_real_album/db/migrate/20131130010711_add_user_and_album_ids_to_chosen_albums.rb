class AddUserAndAlbumIdsToChosenAlbums < ActiveRecord::Migration
  def up
    add_column :chosen_albums, :user_id, :integer
    add_column :chosen_albums, :album_id, :integer
    add_index :chosen_albums, :user_id
    add_index :chosen_albums, :album_id
  end
  def down
    remove_column :chosen_albums, :user_id
    remove_column :chosen_albums, :album_id
  end
end
