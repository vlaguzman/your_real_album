class CreateAlbums < ActiveRecord::Migration
  def up
    create_table :albums do |t|
      t.string :title
      t.string :description 
    end
  end

  def down
    drop_table :albums
  end
end
