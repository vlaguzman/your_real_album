class AddAlbumIdToRules < ActiveRecord::Migration
  def up
    add_column :rules, :album_id, :integer
    add_index :rules,  :album_id
  end
  def down
    remove_column :rules, :album_id
  end
end
