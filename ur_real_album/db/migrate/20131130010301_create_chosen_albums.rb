class CreateChosenAlbums < ActiveRecord::Migration
  def up
    create_table :chosen_albums do |t|
      t.string :state
    end
  end

  def down
    drop_table :chosen_albums
  end
end
