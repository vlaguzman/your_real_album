class ChosenAlbum < ActiveRecord::Base
  attr_accessible :state, :user, :album
  validates :state, presence: true

  belongs_to :user
  belongs_to :album
end
