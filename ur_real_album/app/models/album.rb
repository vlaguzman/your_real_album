class Album < ActiveRecord::Base
  attr_accessible :title, :description
  validates :title, presence: true, uniqueness: true
  validates :description, presence: true

  has_many :stickers
  has_many :rules
  has_many :chosen_albums
  has_many :users, through: :chosen_albums
end

