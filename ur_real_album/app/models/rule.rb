class Rule < ActiveRecord::Base
  attr_accessible :name, :description
  validates :name, presence: true
  validates :description, presence: true

  belongs_to :album
  belongs_to :user
end
