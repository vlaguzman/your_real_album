class User < ActiveRecord::Base
  attr_accessible :name, :email, :password
  validates :name, presence: true
  validates :email, presence: true
  validates :password, presence: true

  has_many :rules
  has_many :chosen_albums
  has_many :albums, through: :chosen_albums
end
