class Sticker < ActiveRecord::Base
  attr_accessible :name, :image
  validates :name, presence: true

  belongs_to :album
end
