module UserHelper
  def add_album(album, user)
    unless user.albums.include?(album)
      chosen_album = ChosenAlbum.new(user: user, album: album, state: "selected")
      album.chosen_albums << chosen_album
      user.chosen_albums << chosen_album
      chosen_album.save!
      user.save!
      album.save!
    else 
      raise "This album has been already selected"
    end  
  end
end
