class RulesController < ApplicationController

  def new
    @rule = Rule.new
  end

  def create
    @rule = Rule.new(params[:rule])
    if @rule.save
      redirect_to @rule
    else
      render 'new'
    end
  end

  def show
    @rule = Rule.find(params[:id])
  end

end
