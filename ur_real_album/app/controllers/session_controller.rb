class SessionController
  attr_accessor :users

  def initialize
    @users = Array.new
  end

  def add_user(user)
    @users << user
  end

  def is_valid?(email, password)
    user = users.detect{|a| a.email == email}
    !user.nil? ? (user.password == password ? true : false) : false
  end
end
